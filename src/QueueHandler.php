<?php

namespace Archaic\Http;

use Archaic\Log\SimpleMessageFormatter;
use Archaic\Log\StreamLogger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;


class QueueHandler implements RequestHandlerInterface {

  /** @var MiddlewareInterface|RequestHandlerInterface[] $queue */
  private array $queue;

  private StreamLogger $logger;

  /** @param MiddlewareInterface|RequestHandlerInterface[] $queue */
  public function __construct(array $queue) {
    $this->queue = $queue;
    $this->index = 0;

    $this->logger = new StreamLogger(new SimpleMessageFormatter(self::class));
  }

  public function handle(ServerRequestInterface $request): ResponseInterface {
    $handler = @$this->queue[$this->index];

    if ($handler === null) {
      $this->logger->criticalf("last element in queue does implement %s", RequestHandlerInterface::class);

    } else if ($handler instanceof MiddlewareInterface) {
      $this->index += 1;
      return $handler->process($request, $this);

    } else if ($handler instanceof RequestHandlerInterface) {
      return $handler->handle($request);

    } else {
      $this->logger->criticalf("queue[%d] does not implement %s neither %s", 
        $this->index, RequestHandlerInterface::class, MiddlewareInterface::class);
    }
  }
}
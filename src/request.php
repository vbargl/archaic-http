<?php

namespace Archaic\Http;

use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use Psr\Http\Message\ServerRequestInterface;

function requestFromGlobals(): ServerRequestInterface {
  $f = new Psr17Factory();
  return (new ServerRequestCreator($f, $f, $f, $f))->fromGlobals();
}
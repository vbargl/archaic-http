<?php

namespace Archaic\Http;

use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Http\Message\ResponseInterface;

function emit(ResponseInterface $response) {
  (new SapiEmitter)->emit($response);
}
<?php

namespace Archaic\Http;

use Psr\Http\Message\RequestInterface;

use function Archaic\Log\log;
use function Archaic\Log\printf;

function query(RequestInterface $request, string $name): ?string {
  $rawQuery = $request->getUri()->getQuery();
  $pairs = explode('&', $rawQuery);

  foreach ($pairs as $pair) {
    $keyval = explode('=', $pair, 2);
    if ($keyval[0] == $name) {
      return $keyval[1];
    }
  }

  return null;
}

function queries(RequestInterface $request, array $filter = []): array {
  $rawQuery = $request->getUri()->getQuery();
  $pairs = explode('&', $rawQuery);

  $arr = [];
  foreach ($pairs as $pair) {
    $keyval = explode('=', $pair, 2);
    $arr[$keyval[0]] = $keyval[1] ?? '';
  }

  if (!empty($filter)) {
    $arr = array_intersect_key($arr, array_flip($filter));
  }
  
  return $arr;
}

function queriesList(RequestInterface $request, array $filter = []): array {
  $arr = array_values(queries($request, $filter));

  for ($missing = count($filter) - count($arr); $missing > 0; $missing--) {
    $arr[] = null;
  }
  
  return $arr;
}
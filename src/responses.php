<?php

namespace Archaic\Http;

use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

function json($data, int $status = 200, bool $pretty = false): ResponseInterface {
  return new Response($status, 
    /* header */ ['content-type' => 'application/json'], 
    /* body   */ json_encode($data, $pretty ? JSON_PRETTY_PRINT : 0)
  );
}

function redirect(string $location, int $status = 301): ResponseInterface {
  return new Response($status, compact('location'));
}

function redirectFound(string $location): ResponseInterface {
  return new Response(302, compact('location'));
}
